package com.siossae.android.materialdesign;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView hrg_kolak, hrg_sumsum, hrg_doger, hrg_buah;
    private CheckBox cek_kolak, cek_sumsum, cek_doger, cek_buah;
    private Button htg_harga;
    int harga_kolak, harga_sumsum, harga_doger, harga_buah, total_harga;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initId();

    }

    public void initId() {
        total_harga = 0;
        harga_kolak = 3000;
        harga_sumsum = 5000;
        harga_doger = 2000;
        harga_buah = 2500;

        hrg_kolak = (TextView) findViewById(R.id.hrg_kolak);
        hrg_sumsum = (TextView) findViewById(R.id.hrg_sumsum);
        hrg_doger = (TextView) findViewById(R.id.hrg_doger);
        hrg_buah = (TextView) findViewById(R.id.hrg_buah);

        hrg_kolak.setText(idrFormat(harga_kolak));
        hrg_sumsum.setText(idrFormat(harga_sumsum));
        hrg_doger.setText(idrFormat(harga_doger));
        hrg_buah.setText(idrFormat(harga_buah));

        cek_kolak = (CheckBox) findViewById(R.id.cek_kolak);
        cek_sumsum = (CheckBox) findViewById(R.id.cek_sumsum);
        cek_doger = (CheckBox) findViewById(R.id.cek_doger);
        cek_buah = (CheckBox) findViewById(R.id.cek_buah);

        cek_kolak.setOnClickListener(this);
        cek_sumsum.setOnClickListener(this);
        cek_doger.setOnClickListener(this);
        cek_buah.setOnClickListener(this);

        htg_harga = (Button) findViewById(R.id.htg_harga);
        htg_harga.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v == cek_kolak) {
            if (cek_kolak.isChecked()) {
                total_harga = total_harga + harga_kolak;
            } else {
                total_harga = total_harga - harga_kolak;
            }
        }

        if (v == cek_sumsum) {
            if (cek_sumsum.isChecked()) {
                total_harga = total_harga + harga_sumsum;
            } else {
                total_harga = total_harga - harga_sumsum;
            }
        }

        if (v == cek_doger) {
            if (cek_doger.isChecked()) {
                total_harga = total_harga + harga_doger;
            } else {
                total_harga = total_harga - harga_doger;
            }
        }

        if (v == cek_buah) {
            if (cek_buah.isChecked()) {
                total_harga = total_harga + harga_buah;
            } else {
                total_harga = total_harga - harga_buah;
            }
        }

        if (v == htg_harga) {
            Intent itn = new Intent(MainActivity.this, SecondActivity.class);
            itn.putExtra("ttl", idrFormat(total_harga));
            startActivity(itn);
            //Toast.makeText(getApplicationContext(), idrFormat(total_harga), Toast.LENGTH_SHORT).show();
        }
    }

    public String idrFormat (int harga) {
        DecimalFormat df = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setCurrencySymbol("IDR ");
        dfs.setGroupingSeparator('.');
        dfs.setMonetaryDecimalSeparator(',');
        df.setDecimalFormatSymbols(dfs);
        return df.format(harga);
    }
}
