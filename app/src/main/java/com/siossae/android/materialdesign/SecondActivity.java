package com.siossae.android.materialdesign;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by pri on 14/06/16.
 */
public class SecondActivity extends AppCompatActivity {

    TextView tx1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        tx1 = (TextView) findViewById(R.id.ttl_harga);

        Intent itn = getIntent();
        String harga_total = itn.getStringExtra("ttl");
        tx1.setText("Total Harga: " + harga_total);
    }
}
